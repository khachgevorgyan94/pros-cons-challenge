import React, {Component} from 'react';

import './style.scss';
import GroupController from "../../platform/api/group";
import UserController from "../../platform/api/user";
import MainController from "../../platform/api/main-data";
import PageLoader from "../../components/page-loader";
import ProsConsBox from "./compoents/pros-cons-box";
import {ProsConsType} from "../../platform/enum/pros-cons-type";


class Main extends Component {
  state = {
    data: null,
    inProgress: true
  }

  componentDidMount = async () => {
    await this.getId()
  }

  getId = async () => {
    const groupId = await GroupController.getGroupId();
    const userId = await UserController.getUserId();
    const promise = [groupId, userId];
    this.setState({inProgress: true}, async () => {
      Promise.all(promise).then(response => {
        if (response[0].groupId && response[1].userId) {
          this.getData(response[0].groupId, response[1].userId)

        }
      })
    })

  }

  getData = async (groupId, userId) => {
    const result = await MainController.getData(groupId, userId)
    if (result) {
      this.setState({
        data: result,
        inProgress: false,
      })

    }
  }
  updateData = (newData, type) => {

    const {data} = this.state;
    if (type === ProsConsType.Pros) {
      data.pros = newData
    } else {
      data.cons = newData
    }
    this.setState({data})
  }

  render() {
    const {inProgress, data} = this.state
    return (
        <div className='G-container'>
          {data && <div className='P-main-block G-flex G-align-start'>
            <ProsConsBox title='Pro`s'
                         data={data.pros}
                         type={ProsConsType.Pros}
                         updateData={(data, type) => this.updateData(data, type)}
            />
            <ProsConsBox title='Con`s'
                         data={data.cons}
                         type={ProsConsType.Cons}
                         updateData={(data, type) => this.updateData(data, type)}
            />
          </div>}
          {inProgress ? <PageLoader/> : null}
        </div>
    );
  }
}

export default Main;


