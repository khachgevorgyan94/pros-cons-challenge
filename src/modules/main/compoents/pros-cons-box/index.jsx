import React, {Component} from 'react';

import addImage from '../../../../assets/images/add.png'
import removeImage from '../../../../assets/images/remove.png'
import {ProsConsType} from "../../../../platform/enum/pros-cons-type";
import Modal from "../../../../components/modal";
import ModalUpdate from "../../../../components/modal-update";


class ProsConsBox extends Component {
  state = {
    text: '',
    isOpenModal: false,
    updateText: {
      text: '',
      index: null
    }
  }

  inputChange = (e) => {
    this.setState({text: e.currentTarget.value})
  }

  addData = () => {
    const {updateData, data, type} = this.props;
    if (this.state.text.length && data) {
      data.push(this.state.text)
      updateData(data, type)
      this.setState({
        text: ''
      })
    }
  }

  deleteData = (e, text) => {
    e.stopPropagation();
    e.preventDefault();
    const {updateData, data, type} = this.props;
    const index = data.indexOf(text);
    if (index >= 0) {
      data.splice(index, 1)
    }
    updateData(data, type)
  }

  openModal = (text, index) => {
    this.setState({
      isOpenModal: true,
      updateText: {
        text,
        index
      }
    })
  }
  closeModal = () => {
    this.setState({
      isOpenModal: false
    })
  }

  updateText = (newText) => {
    const {updateText} = this.state
    const {data, type, updateData} = this.props
    let newData = data.map((item, index) => {
      if (index === updateText.index) {
        return item = newText
      }else return item
    })
    updateData(newData, type)

  }
  handleKeyPress = e => {
    if (e.key === 'Enter') this.addData();
  }

  render() {
    const {title, data, type} = this.props;
    return (
        <div className='P-main-box'>
          <div className='P-main-box-header'>
            <h3>{title}</h3>
          </div>
          <div className='P-main-box-body'>
            {data.length ? data.map((item, index) => {
              return <div key={index} className='P-message-box'>
                <div className='P-message-text G-flex G-align-center' onClick={() => this.openModal(item, index)}>
                  <p>{item}</p>
                  <span onClick={(e) => this.deleteData(e, item)} className='P-delete-icon'
                        style={{backgroundImage: `url('${removeImage}')`}}/>
                </div>
              </div>
            }) : <p className='P-empty-list'>List was empty</p>}
          </div>
          <div className='P-main-box-footer'>
            <div className='P-input-block G-flex G-align-center'>
              <label>
                <input value={this.state.text} onChange={this.inputChange}
                       placeholder={type === ProsConsType.Pros ? 'New Pro`s' : 'New Con`s'}
                       onKeyPress={this.handleKeyPress}
                       type="text"/>
              </label>
              <span onClick={this.addData} className='P-add-icon' style={{backgroundImage: `url('${addImage}')`}}/>
            </div>
          </div>
          <Modal isOpen={this.state.isOpenModal}
                 close={this.closeModal}>
            <ModalUpdate  close={this.closeModal} text={this.state.updateText.text}
                         updateText={(text) => this.updateText(text)}/>
          </Modal>
        </div>

    );
  }
}

export default ProsConsBox;


