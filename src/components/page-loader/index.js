import React, {PureComponent} from 'react';

import './style.scss';

class PageLoader extends PureComponent {

  state = {show: true};


  render() {
    const {show} = this.state;

    return show ? (
        <div className="M-loader-wrapper">
          <div className="lds-roller">
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
            <div/>
          </div>
        </div>
    ) : null;
  }
}

export default PageLoader;
