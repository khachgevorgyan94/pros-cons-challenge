import React, {Component} from 'react';

import './style.scss';


class ModalUpdate extends Component {

  state = {
    text: '',
    errorText: false,

  }

  componentDidMount() {
    this.setState({
      text: this.props.text
    })
  }

  inputChange = (e) => {
    this.setState({text: e.currentTarget.value})
  }
  saveText = () => {
    const {updateText, close} = this.props
    if (this.state.text && this.state.text.split('').length) {
      updateText(this.state.text)
      close()
    } else {
      this.setState({
        errorText: true
      })
    }
  }

  handleKeyPress = e => {
    if (e.key === 'Enter') this.saveText();
  }
  render() {
    const {close} = this.props
    const {errorText, text} = this.state
    return (
        <div className='P-update-modal-block'>
          <h3 className='P-title'>Update description</h3>
          <div className={`P-input-block ${errorText ? 'P-error' : ''}`}>
            <label>
              <input placeholder='Edit text'
                     value={text}
                     type="text"
                     onKeyPress={this.handleKeyPress}

                     onChange={this.inputChange}/>
            </label>
          </div>
          <div className='P-update-modal-button G-flex G-align-center G-justify-center'>
            <p onClick={close}>Cancel</p>
            <div className='P-accept-btn'>
              <button onClick={this.saveText}>Save</button>
            </div>
          </div>
        </div>
    );
  }
};

export default ModalUpdate;
