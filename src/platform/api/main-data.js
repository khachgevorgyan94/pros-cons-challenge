import Connection from '../serivces/connection';

const controllerName = 'proscons';

class MainController {

  static getData = (groupId, userId) => {
    const result = Connection.GET(controllerName, `group/${groupId}/user/${userId}`);
    return result;
  };
}

export default MainController;

